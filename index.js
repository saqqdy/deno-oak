const { cwd, stdout, copy } = Deno;
import { Application } from "https://deno.land/x/oak/mod.ts";
// import { proxy } from "https://deno.land/x/oak_http_proxy/mod.ts";
import {
	WebSocket,
	WebSocketServer,
} from "https://deno.land/x/websocket@/mod.ts";
import { create, verify } from "https://deno.land/x/djwt/mod.ts";
// import watch from "https://deno.land/x/watch/mod.ts";
import config from "./config/dev.js";

// import * as uuid from "https://deno.land/x/uuid/genuuid.ts";
// import { parse, stringify } from "https://denolib.com/denolib/qs/mod.ts";
import * as dejs from "https://deno.land/x/dejs/mod.ts";
// import parseXML from "https://denopkg.com/nekobato/deno-xml-parser/index.ts";
const { renderFile } = dejs;
// console.log(12121, uuid)

// WebSocketServer
// const wss = new WebSocketServer(8080);
// wss.on("connection", function (ws) {
// 	ws.on("message", function (message) {
// 		console.log(message);
// 		ws.send(message);
// 	});
// });

// DJWT
const jwt = await create(
	{ alg: "HS512", typ: "JWT" },
	{ foo: "bar" },
	"secret"
);
const payload = await verify(jwt, "secret", "HS512"); // { foo: "bar" }
console.log(jwt, payload);

// Watch
// for await (const changes of watch("./")) {
// 	console.log(changes.added);
// 	console.log(changes.modified);
// 	console.log(changes.deleted);
// }
// const end = watch("src").start((changes) => {
// 	console.log(changes);
// });
// console.log(end)

const app = new Application();

// parseXML
// (async function main() {
// 	const node = parseXML("<foo>hello world</foo>");
// 	console.log(node);
// })()

// (async () => {
// 	const output = await renderFile(`${cwd()}/template.ejs`, {
// 		name: "world111",
// 	});
// 	await copy(output, stdout);
// })();

// Hello World!
app.use(async (ctx) => {
	const output = await renderFile(`${cwd()}/template.ejs`, {
		name: "world111",
	});
	ctx.response.body = output;
	// ctx.response.body =
	// 	"Hello World!\n" +
	// 	JSON.stringify(userInfo) +
	// 	"\n" +
	// 	JSON.stringify(users) +
	// 	"\n" +
	// 	JSON.stringify(all_users);
});

// app.use(proxy("https://github.com/oakserver/oak"));
// router.get("/string", proxy("http://egg.saqqdy.com/common/list"));
// router.get(
// 	"/function",
// 	proxy((ctx) => new URL("http://egg.saqqdy.com/common/list"))
// );

await app.listen({ port: 5001 });
