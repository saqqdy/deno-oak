import { Client } from "https://deno.land/x/mysql/mod.ts";
import config from "../../config/dev.js";

const mysqlClient = await new Client().connect({
	hostname: config.mysql.host,
	username: config.mysql.user,
	db: config.mysql.db,
	pool: 3, // connection limit
	password: config.mysql.pass,
	debug: true,
});

export default mysqlClient;
