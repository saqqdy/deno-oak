import { connect } from "https://deno.land/x/redis/mod.ts";
import config from "../../config/dev.js";

const redisClient = await connect({
	hostname: config.redis.host,
	port: config.redis.port,
	password: config.redis.pass,
	db: config.redis.db,
});

export default redisClient;
