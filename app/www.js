import { listenAndServe } from "https://deno.land/std/http/server.ts";
import { blue, green, red, yellow } from "https://deno.land/std/fmt/colors.ts";
import app from "./app.js";
// import webSocket from "./io/connection.js";

// webSocket.on("connection", function (ws) {
// 	console.log("socket connected!");
// 	ws.on("message", function (message) {
// 		console.log(message);
// 		ws.send(message);
// 	});
// });

await app.listen({ port: 5001 });
console.info("Server started at http://127.0.0.1:5001");
// 在不希望应用程序监听 std/http/server 上的请求，但仍希望应用程序处理请求的情况下，可以使用 .handle() 方法。例如，如果你在一个无服务器的函数中，请求是以不同的方式到达的。.handle()方法会调用中间件，就像中间件会被.listen()处理的每个请求所调用一样。它最多需要两个参数，一个是符合std/http/server的ServerRequest接口的请求，另一个是可选的第二个参数，即请求是否是 "安全 "的，即它是否来自于与远程客户端的TLS连接。该方法解析的响应符合ServerResponse接口（可与std/http/server的request.response()方法一起使用），如果ctx.response === true，则为未定义（例如，连接升级到了web套接字，或者正在发回服务器发送的事件）
// await listenAndServe({ port: 5001 }, async (request) => {
// 	const response = await app.handle(request);
// 	if (response) {
// 		request.respond(response);
// 	}
// 	console.info("Server started at http://127.0.0.1:5001");
// });
