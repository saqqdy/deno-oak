// import { send } from "https://deno.land/x/oak/mod.ts";
import { success, error } from "./base.js";
// import config from "../../config/dev.js";
import UsersService from "../service/users.js";

export default {
	/**
	 * @api {get} /api/users/getUserList 获取列表
	 * @apiGroup Users
	 * @apiParam current 当前页码
	 * @apiParam limit 当前显示个数
	 * @apiParam keyword 关键词
	 * */
	getUserList: async (ctx) => {
		const data = await UsersService.list();
		if (data) {
			success(ctx, data);
		} else {
			error(ctx, {
				msg: "出错了",
			});
		}
	},
	/**
	 * @api {get} /api/users/getUserDetail 查看文章
	 * @apiGroup Users
	 * @apiParam id {String} ID
	 */
	getUserDetail: async (ctx) => {
		const data = await UsersService.detail({
			gitName: "saqqdy",
		});
		if (data) {
			success(ctx, data);
		} else {
			error(ctx, {
				msg: "出错了",
			});
		}
	},
};
