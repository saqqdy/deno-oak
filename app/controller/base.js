/**
 * @description 成功回调
 * @param ctx
 * @param data
 */
function success(ctx, data) {
	ctx.response.status = 200;
	ctx.response.body = {
		success: true,
		code: 0,
		data,
		msg: "成功",
	};
}

/**
 * @description 失败回调
 * @param ctx
 * @param data
 */
function error(ctx, { code = -1, msg, status = 500 }) {
	// ctx.response.status = status
	ctx.response.body = {
		success: false,
		code,
		msg,
	};
}

export { success, error };
