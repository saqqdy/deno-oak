import { Bson, MongoClient } from "https://deno.land/x/mongo@v0.22.0/mod.ts";
import config from "../../config/dev.js";

const mongoClient = new MongoClient();
await mongoClient.connect(
	`mongodb://${config.mongo.host}:${config.mongo.port}`
);
const db = mongoClient.database(config.mongo.db, {
	user: config.mongo.user,
	pass: config.mongo.pass,
	auth: { authSource: config.mongo.auth },
	useUnifiedTopology: config.mongo.useUnifiedTopology,
});

const SlowApi = db.collection("help_slow_api");
const Users = db.collection("help_users");

export default db;
export { db, SlowApi, Users };
