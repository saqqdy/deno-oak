export default {
	/**
	 * @description 分页查询列表
	 * @param {Object} model 数据库
	 * @param {Object} filter 筛选
	 * @return {Promise<unknown>} 数组
	 */
	list(model, filter = {}) {
		return model
			.find({
				// url: { $ne: null },
			})
			.toArray();
		// return new Promise((resolve, reject) => {
		// 	let {
		// 			limit = 20,
		// 			current = 1,
		// 			keywordKey = "name",
		// 			keyword,
		// 			populate = [],
		// 			conditions = {},
		// 			sort = {},
		// 		} = filter,
		// 		sortMap = {};
		// 	populate = populate.map((item) => ({
		// 		path: item,
		// 	}));
		// 	if (keyword) {
		// 		conditions.$or = [];
		// 		[].concat(keywordKey).forEach((key) => {
		// 			conditions.$or.push({
		// 				[key]: new RegExp(decodeURIComponent(keyword)),
		// 			});
		// 		});
		// 	}
		// 	sortMap = Object.assign(
		// 		{
		// 			createTime: 1,
		// 		},
		// 		sort
		// 	);
		// 	if (limit === -1) {
		// 		// 查询全部
		// 		model
		// 			.find(conditions)
		// 			.sort(sortMap)
		// 			.populate(populate)
		// 			.exec((err, res) => {
		// 				if (err) {
		// 					reject(err);
		// 					return;
		// 				}
		// 				resolve({
		// 					total: res.length,
		// 					totalPage: 1,
		// 					current,
		// 					list: res,
		// 				});
		// 			});
		// 		return;
		// 	}
		// 	const start = (current - 1) * limit;
		// 	model
		// 		// .estimatedDocumentCount(conditions)
		// 		.countDocuments(conditions)
		// 		.populate(populate)
		// 		.exec((err, count) => {
		// 			if (count < start || count === 0) {
		// 				resolve({
		// 					total: count,
		// 					totalPage: Math.ceil(count / limit),
		// 					current,
		// 					list: [],
		// 				});
		// 				return;
		// 			}
		// 			model
		// 				.find(conditions)
		// 				.sort(sortMap)
		// 				.skip(start)
		// 				.limit(limit)
		// 				.populate(populate)
		// 				.exec((err, res) => {
		// 					if (err) {
		// 						reject(err);
		// 						return;
		// 					}
		// 					resolve({
		// 						total: count,
		// 						totalPage: Math.ceil(count / limit),
		// 						current,
		// 						list: res,
		// 					});
		// 				});
		// 		});
		// });
	},
	/**
	 * @description 读总数
	 * @param {Object} model 数据库
	 * @param {Object} conditions 筛选
	 * @return {Promise} 数据
	 */
	count(model, conditions) {
		return new Promise((resolve, reject) => {
			model.countDocuments(conditions).exec((err, count) => {
				if (err) {
					reject(err);
					return;
				}
				resolve(count);
			});
		});
	},
	/**
	 * @description 查询详情
	 * @param {Object} model 数据模型
	 * @param {Object} filter 筛选
	 * @param {Array} populate 数据
	 * @return {Promise} 返回对象
	 */
	detail(model, filter, populate = []) {
		return model.findOne(filter);
		// return new Promise((resolve, reject) => {
		// 	populate = populate.map((item) => ({
		// 		path: item,
		// 	}));
		// 	model
		// 		.findOne(filter)
		// 		.populate(populate)
		// 		.exec((err, res) => {
		// 			if (err) {
		// 				reject(err);
		// 				return;
		// 			}
		// 			resolve(res);
		// 		});
		// });
	},
	/**
	 * @description 新增
	 * @param {Object} model 数据模型
	 * @param {Object} data 数据
	 * @return {Promise} 返回对象
	 */
	create(model, data) {
		return new Promise((resolve, reject) => {
			new model(data).save((err, res) => {
				if (err) {
					reject(err);
					return;
				}
				resolve(res);
			});
		});
	},
	/**
	 * @description 修改
	 * @param {Object} model 数据模型
	 * @param {Object} filter 筛选
	 * @param {Object} data 数据
	 * @return {Promise} 返回对象
	 */
	update(model, filter, data) {
		return new Promise((resolve, reject) => {
			model.findOne(filter).updateOne(data, (err, res) => {
				if (err) {
					reject(err);
					return;
				}
				resolve({
					status: Boolean(res.ok),
					successNum: res.n,
					failNum: res.nModified,
				});
			});
		});
	},
	/**
	 * @description 删除
	 * @param {Object} model 数据模型
	 * @param {Object} filter 数据
	 * @return {Promise} 返回对象
	 */
	delete(model, filter) {
		return new Promise((resolve, reject) => {
			model.remove(filter, (err, res = true) => {
				if (err) {
					reject(err);
					return;
				}
				resolve(res);
			});
		});
	},
};
