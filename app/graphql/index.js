import { Router } from "https://deno.land/x/oak/mod.ts";
import {
	applyGraphQL,
	gql,
	GQLError,
} from "https://deno.land/x/oak_graphql/mod.ts";

const types = gql`
	type User {
		firstName: String
		lastName: String
	}
	input UserInput {
		firstName: String
		lastName: String
	}
	type ResolveType {
		done: Boolean
	}
	type Query {
		getUser(id: String): User
	}
	type Mutation {
		setUser(input: UserInput!): ResolveType!
	}
`;

const resolvers = {
	Query: {
		getUser: (parent, { id }, context, info) => {
			console.log("id", id, context);
			if (context.user === "Aaron1") {
				throw new GQLError({ type: "auth error in context" });
			}
			return {
				firstName: "wooseok",
				lastName: "lee",
			};
		},
	},
	Mutation: {
		setUser: (
			parent,
			{ input: { firstName, lastName } },
			context,
			info
		) => {
			console.log("input:", firstName, lastName);
			return {
				done: true,
			};
		},
	},
};

const GraphQLService = await applyGraphQL({
	Router,
	typeDefs: types,
	resolvers: resolvers,
	context: (ctx) => {
		console.log(111, ctx);
		return { user: "Aaron" };
	},
});

export default GraphQLService;
