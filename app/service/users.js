import { v4 } from "https://deno.land/std/uuid/mod.ts";
import config from "../../config/dev.js";
import { Users } from "../mongo/connection.js";
import redis from "../redis/connection.js";
// import mysql from "../mysql/connection.js";
import mongoose from "../mongo/mongoose.js";

export default {
	/**
	 * @description 查询列表
	 * @param {Object} filter 筛选
	 * @return {Array} 列表
	 */
	async list(filter) {
		// Redis
		await redis.set("saqqdy", "888888");
		const userInfo = await redis.get("16082914968a6ffd837bee1b4764fffb");
		const saqqdy = await redis.get("saqqdy");
		console.log("redis: ", saqqdy, userInfo);
		// MySQL
		// const users = await mysql.query(`select * from users`);
		// console.log('mysql: ', users);
		// UUID
		console.log(v4.generate());

		const data = await mongoose.list(Users, filter);
		return data;
	},
	/**
	 * @description 查询详情
	 * @param {Object} filter 筛选
	 * @param {Object} populate populate
	 * @return {Object} 详情
	 */
	async detail(filter, populate) {
		const data = await mongoose.detail(Users, filter, populate);
		return data;
	},
	/**
	 * @description 新增
	 * @param {Object} data 数据
	 * @return {Object} 返回
	 */
	async add(data) {
		const detail = await mongoose.create(Users, data);
		return detail;
	},
	/**
	 * @description 新增
	 * @param {Object} filter 筛选
	 * @param {Object} data 数据
	 * @return {Object} 返回
	 */
	async edit(filter, data) {
		const detail = await mongoose.update(Users, filter, data);
		return detail;
	},
	/**
	 * @description 删除
	 * @param {Object} filter 筛选
	 * @return {Boolean} 返回操作状态
	 */
	async del(filter) {
		const status = await mongoose.delete(Users, filter);
		return status;
	},
};
