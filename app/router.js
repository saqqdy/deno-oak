import { Router, helpers, send } from "https://deno.land/x/oak/mod.ts";
import { proxy } from "https://deno.land/x/oak_http_proxy/mod.ts";
import { multiParser } from "https://deno.land/x/multiparser/mod.ts";
// import {
// 	validatorMiddleware,
// 	ValidatorMiddlewareOptions,
// } from "https://raw.githubusercontent.com/halvardssm/oak-middleware-validator/master/mod.ts";
import UsersController from "./controller/users.js";
const { getQuery } = helpers;
const router = new Router();

// fakeData
const books = new Map();
books.set("100", {
	id: "100",
	title: "The Hound of the Baskervilles",
	author: "Conan Doyle, Arthur",
});

router
	.get("/users/getUserList", UsersController.getUserList)
	.get(
		"/users/getUserDetail",
		// validatorMiddleware({
		// 	validations: [
		// 		{ key: "a", validationOption: "string", isUrlParam: true },
		// 		{ key: "b", validationOption: "b", isOptional: true },
		// 		// { key: "c", validationOption: validateStringIn("b") },
		// 		// { key: "d", validationOption: validateIsNumber(0, 1) },
		// 	],
		// 	bodyRequired: true,
		// 	errorMessages: {
		// 		ERROR_NO_BODY: "Custom message",
		// 	},
		// }),
		UsersController.getUserDetail
	)
	.get(
		"/system/getImageInfo",
		proxy("http://mhwy.kingdee.com/jar/system/getImageInfo")
	)
	.get("/", async (ctx) => {
		// ctx.response.body = "Hello world!";
		await send(ctx, ctx.request.url.pathname, {
			root: `${Deno.cwd()}/app/views`,
			index: "index.html",
		});
	})
	.get("/reader", async (ctx) => {
		const result = ctx.request.body({ type: "reader" });
		result.type; // "reader"
		await Deno.readAll(result.value); // a "raw" Uint8Array of the body
	})
	// 类型选项的另一个用例是，如果某些中间件总是需要特定格式的主体，但希望其他中间件以内容类型解析的方式消费它。
	.get("/hasBody", async (ctx) => {
		if (ctx.request.hasBody) {
			const result = ctx.request.body({ type: "text" });
			const text = await result.value;
			// do some validation of the body as a string
		} else {
			const result = ctx.request.body(); // content type automatically detected
			if (result.type === "json") {
				const value = await result.value; // an object of parsed JSON
			}
		}
	})
	// .get("/text", async (ctx) => {
	// 	const result = ctx.request.body({
	// 		contentTypes: {
	// 			text: ["application/javascript"],
	// 		},
	// 	});
	// 	result.type; // "text"
	// 	await result.value; // a string containing the text
	// })
	.get("/test", (ctx) => {
		// type contains the type of "json", "text", "form", "form-data", "raw" or "undefined"
		// const {
		// 	app,
		// 	cookies,
		// 	request,
		// 	response,
		// 	respond,
		// 	socket,
		// 	state,
		// 	assert,
		// 	send,
		// 	sendEvents,
		// 	throw,
		// 	upgrade,
		// } = ctx;
		// ctx.cookies.get() ctx.cookies.set()
		// const { hasBody, headers, method, secure, serverRequest, url, accepts, acceptsCharsets, acceptsEncodings, acceptsLanguages, body } = ctx.response;
		// const { body, headers, status, type, redirect } = ctx.response;
		ctx.response.body = Array.from(books.values());
	})
	.get("/query/:id", (ctx) => {
		const query = getQuery(ctx, { mergeParams: true, asMap: false }); // ctx.params.id
		console.log(query);
		if (ctx.params && ctx.params.id && books.has(ctx.params.id)) {
			ctx.response.body = books.get(ctx.params.id);
		} else {
			ctx.response.body = "test id";
		}
	});

export default router;
