import {
	WebSocketClient,
	WebSocketServer,
} from "https://deno.land/x/websocket/mod.ts";

const wss = new WebSocketServer();

export default wss
